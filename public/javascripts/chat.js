let searchParams = new URLSearchParams(window.location.search);
let roomId = searchParams.get('roomId');
socket = io.connect("http://localhost:3000");
socket.emit('room',roomId);

$('#button').click(function (){
    socket.emit('message',{data:$('#message').val()});
    $('#message').val(``);
});
socket.on('message',function (data){
    if(data.purpose)
        $('#count').html(data.message);
    else
        $('#container').append(`<div style="width:750px;color:white;font-size: medium;border-radius: 10px;background-color: lightskyblue"><span style="margin-left: 10px;"><b>${data.userName}</b>: ${data.message}</span></div><br>`);
});
socket.on('user-connect',function (data){
    $('#snackbar').html(`${data.userName} joined the room`);
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    $('#users-box').append(`<p id=${data.userId}>${data.userName}</p>`)
});
$('#message').focus(function(){
    socket.emit('typing');
});
$('#message').blur(function(){
    socket.emit('not-typing');
});

socket.on('typing',function(){
    $('#typing').html('typing...');
});
socket.on('not-typing',function(){
    $('#typing').html('');
});
ex
// socket.on('users-count-connect',function(data){
//    $('#count').html(data.count);
// });
// socket.on('users-count-disconnect',function(data){
//    $('#count').html(data.count);
// });
