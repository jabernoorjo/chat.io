class Chat{
    constructor(){

    }
    sendSystemMessage(message, socket){
        socket.emit('message', {
            for: 'everyone',
            message: message,
            userName: 'System'
        });
    };
    emitTyping(socket,roomId){
        socket.on('typing', () => {
            socket.to(roomId).emit('typing');
        });
    };
    emitNotTyping(socket,roomId){
        socket.on('not-typing', () => {
            socket.to(roomId).emit('not-typing');
        });
    };
    emitUserConnect(socket,roomId,data){
        socket.to(roomId).emit('user-connect', data);
    };
    emitUserDisconnect(socket,io,data){
        socket.on('disconnect', () => {
            console.log('new user disconnected');
            io.send(data);
        });
    };
// {userName: userName, message: data.data}
    emitSendMessage(socket,io,roomId,userName){
        socket.on('message', (data) => {
            io.sockets.of(roomId).emit('message', {userName: userName, message: data.data});
        });
    }

}
module.exports = Chat