let express = require('express');
let router = express.Router();
let loginRouter = require('./login');
let usersRouter = require('./users');
let roomRouter = require('./room');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Login'});
});
router.use('/users', usersRouter);
router.use('/doLogin', loginRouter);
router.use('/room', roomRouter);

module.exports = router;
