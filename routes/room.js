const express = require('express');
const router = express.Router();
const Chat = require('../classes/Chat');
const chat = new Chat();
/* GET home page. */
router.get('/', function (req, res, next) {
    // req.session.hasSession
    if(true) {
        let io  =req.app.get('io');
        let userName =Math.random();
        let socket_id = [];
        let usersCount = 0;
        console.log("outside: "+userName);
        let roomId = req.query.roomId;
        io.on('connection', (socket) => {
            socket_id.push(socket.id);
            // if (socket_id[0] === socket.id) {
            //     // remove the connection listener for any subsequent
            //     // connections with the same ID
            //     io.removeAllListeners('connection');
            // }
            socket.on('room', (id) => {
                socket.join(id, () => {
                    usersCount++;
                    console.log(userName);
                    io.send({purpose: 'count', message: usersCount});
                });
                chat.sendSystemMessage('Hi, welcome to the noor chatting box, please respect each others, and have fun', socket);
                chat.emitTyping(socket,roomId);
                chat.emitNotTyping(socket,roomId);
                chat.emitUserConnect(socket,roomId,{userId:req.session.userId,userName:userName});
                chat.emitUserDisconnect(socket,io,{});
                chat.emitSendMessage(socket,io,roomId,userName);
            });
            // console.log("inside: "+socket.id);
            console.log('new user connected');

        });


        res.render('chat', {title: req.query.roomId});

    }else
        res.status(403).render('access-denied');
});

router.post('/', function (req, res) {
    res.redirect('success');
});

module.exports = router;
