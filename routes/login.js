const express = require('express');
const router = express.Router();
/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('login',{title:"Future"});
});

router.post('/',function(req, res,next) {

    req.session.username = req.body.username;
    req.session.userId = parseInt(Math.random()*100000000);
    req.session.hasSession = true;
    console.log(req.session.username);
    console.log(req.session.userId);

    res.redirect('/');
});

module.exports = router;
